import os
import json
import torch
import imageio
import numpy as np
from torch.utils.data import Dataset, DataLoader


trans_t = lambda t : torch.Tensor([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,1,t],
    [0,0,0,1]]).float()

rot_phi = lambda phi : torch.Tensor([
    [1,0,0,0],
    [0,np.cos(phi),-np.sin(phi),0],
    [0,np.sin(phi), np.cos(phi),0],
    [0,0,0,1]]).float()

rot_theta = lambda th : torch.Tensor([
    [np.cos(th),0,-np.sin(th),0],
    [0,1,0,0],
    [np.sin(th),0, np.cos(th),0],
    [0,0,0,1]]).float()


def pose_spherical(theta, phi, radius):
    c2w = trans_t(radius)
    c2w = rot_phi(phi/180.*np.pi) @ c2w
    c2w = rot_theta(theta/180.*np.pi) @ c2w
    c2w = torch.Tensor(np.array([[-1,0,0,0],[0,0,1,0],[0,1,0,0],[0,0,0,1]])) @ c2w
    return c2w


class BlenderDataset(Dataset):
    def __init__(self, args, mode='train'):
        super().__init__()
        self.datadir = args.datadir
        splits = ['train', 'val', 'test']
        if mode not in splits:
            print(f'no {mode} mode in BlenderDataset')
            raise NotImplementedError

        with open(os.path.join(self.datadir, f'transforms_{mode}.json'), 'r') as fp:
            meta = json.load(fp)

        self.frames = meta['frames']

        _fname = os.path.join(args.datadir, self.frames[0]['file_path'] + '.png')
        _img = imageio.imread(_fname)
        self.H, self.W = _img.shape[:2]

    
    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        frame = self.frames[idx]

        fname = os.path.join(self.datadir, frame['file_path'] + '.png')
        img = imageio.imread(fname)
        img = (np.array(img) / 255.).astype(np.float32)
        pose = np.array(frame['transform_matrix']).astype(np.float32)
        expression = np.array(frame['expression']).astype(np.float32)
        bbox = frame['bbox']
        bbox = [bbox[0]*self.H, bbox[1]*self.H, bbox[2]*self.W, bbox[3]*self.W]
        bbox = [max(bbox[0],0), min(bbox[1], self.H-1), max(bbox[2],0), min(bbox[3], self.W-1)]
        bbox = np.array(bbox).astype(np.int32)

        return img, bbox, pose, expression
    

def get_intrinsics(args):
    with open(os.path.join(args.datadir, f'transforms_val.json'), 'r') as fp:
        meta = json.load(fp)

    frames = meta['frames']
    _fname = os.path.join(args.datadir, frames[0]['file_path'] + '.png')
    _img = imageio.imread(_fname)

    H, W = _img.shape[:2]
    intrinsics = meta['intrinsics']
    fx = intrinsics[0]
    fy = intrinsics[1]
    cx = intrinsics[2]
    cy = intrinsics[2]
    K = np.array([
        [fx, 0, cx*W],
        [0, fy, cy*H],
        [0, 0, 1]
    ])

    return K, H, W


def get_render_poses():
    render_poses = np.stack([pose_spherical(angle, -30.0, 4.0) for angle in np.linspace(-180,180,40+1)[:-1]],0)
    return render_poses



def get_dataloader(args, mode='train'):
    dataset = BlenderDataset(args, mode=mode)
    shuffle = mode == 'train'
    dl = DataLoader(dataset, 
                    batch_size=1, 
                    shuffle=shuffle,
                    num_workers=args.num_workers)
    return dl


'''
from attrdict import AttrDict        


args = AttrDict()
args.num_workers=1
args.datadir = '/DATA/nerface_dataset/dave_dvp/'
dl = get_dataloader(args)
for batch in dl:
    img, bbox, pose, exp  = batch
    print(exp.shape)
    print(type(exp))

    break
'''


