expname = dave
datadir = /DATA/nerface_dataset/dave_dvp/ 

no_batching = True
no_ndc = True

use_viewdirs = True

N_samples = 64
N_importance = 128
N_rand = 2048
