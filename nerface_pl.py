import wandb
import numpy as np
import torch.nn as nn
import pytorch_lightning as pl
from torch.utils.data import DataLoader
from pytorch_lightning.loggers import *

from utils import *
from dataset import get_dataloader, get_intrinsics, get_render_poses


class NerfaceTrainer(pl.LightningModule):
    def __init__(self, args):
        super(NerfaceTrainer, self).__init__()
        self.args = args
        self.automatic_optimization = False

        # create nerf model
        self.render_kwargs_train, self.render_kwargs_test, self.start, self.grad_vars, self.optimizer = create_nerf(args)

        bds_dict = {
            'near': args.near,        
            'far': args.far,
        }

        self.render_kwargs_train.update(bds_dict)
        self.render_kwargs_test.update(bds_dict)

        self.network_fn = self.render_kwargs_train['network_fn']
        self.network_fine = self.render_kwargs_train['network_fine']

        self.K, self.H, self.W = get_intrinsics(args)
        self.render_poses = torch.Tensor(get_render_poses())

        self.N_rand = args.N_rand
        self.use_batching = not args.no_batching
        if self.use_batching:
            raise NotImplementedError


    def forward(self, x, render_kwargs):
        return render(self.H, self.W, self.K, chunk=self.args.chunk, rays=x, verbose=self.global_step < 10, retraw=True, **render_kwargs)


    def configure_optimizers(self):
        return self.optimizer

    def training_step(self, batch, batch_idx):
        target, bbox, pose, expression = batch
        K = self.K
        render_poses = self.render_poses
        pose = pose[0,:3,:4]
        bbox = bbox[0]
        expression = expression[0]
        
        rays_o, rays_d = get_rays(self.H, self.W, self.K, pose, self.device)

        if self.args.bbox_sampling_ratio > 0:
            # sample rays: bbox_sampling_ratio*100 % from bbox
            N_rand_bbox = int(self.N_rand * self.args.bbox_sampling_ratio)
            N_rand_ext = self.N_rand - N_rand_bbox

            coords_bbox = torch.stack(torch.meshgrid(torch.linspace(bbox[0], bbox[1], bbox[1]-bbox[0]+1), torch.linspace(bbox[2], bbox[3], bbox[3]-bbox[2]+1)), -1)
            coords_bbox = torch.reshape(coords_bbox, [-1,2]) # (H'*W', 2)
            coords = torch.stack(torch.meshgrid(torch.linspace(0, self.H-1, self.H), torch.linspace(0, self.W-1, self.W)), -1)
            coords = torch.reshape(coords, [-1,2]) # (H*W, 2)

            combined = torch.cat((coords, coords_bbox))
            uniques, counts = combined.unique(dim=0, return_counts=True)
            coords_ext = uniques[counts == 1]
            #intersection = uniques[counts>1]

            select_inds_bbox = np.random.choice(coords_bbox.shape[0], size=[N_rand_bbox], replace=False)
            select_coords_bbox = coords_bbox[select_inds_bbox].long()

            select_inds_ext = np.random.choice(coords_ext.shape[0], size=[N_rand_ext], replace=False)
            select_coords_ext = coords_ext[select_inds_ext].long()

            select_coords = torch.cat((select_coords_bbox, select_coords_ext))
            rays_o = rays_o[select_coords[:, 0], select_coords[:, 1]] # (N_rand, 3)
            rays_d = rays_d[select_coords[:, 0], select_coords[:, 1]] # (N_rand, 3)
            batch_rays = torch.stack([rays_o, rays_d], 0) # (2, N_rand, 3)
            target_s = target[:, select_coords[:, 0], select_coords[:, 1]] # (b, N_rand, 3)

        else:
            raise NotImplementedError

        expression_dict = {'expression': expression}
        device_dict = {'device': self.device}
        render_kwargs_train = self.render_kwargs_train
        render_kwargs_train.update(expression_dict)
        render_kwargs_train.update(device_dict)
        rgb, disp, acc, extras = self(batch_rays, render_kwargs_train)
        #rgb, disp, acc, extras = render(self.H, self.W, self.K, chunk=self.args.chunk, rays=batch_rays, verbose=self.global_step < 10, retraw=True, **render_kwargs_train)

        img_loss = img2mse(rgb, target_s)
        #trans = extras['raw'][...,-1]
        loss = img_loss
        psnr = mse2psnr(img_loss)

        if 'rgb0' in extras:
            img_loss0 = img2mse(extras['rgb0'], target_s)
            loss = loss + img_loss0
            psnr0 = mse2psnr(img_loss0)

        

        return loss
        


    def train_dataloader(self):
        return get_dataloader(self.args, mode='train')

    def val_dataloader(self):
        return get_dataloader(self.args, mode='val')


        

