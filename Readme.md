# Brain Nerface

위 코드는 nerf-pytorch를 기반으로 작성하였습니다. 
* Nerf-pytorch: <https://github.com/yenchenlin/nerf-pytorch>

* 현재는 forward path 및 training step 부분만 완성된 코드입니다.

* 전체적인 구조를 wav2lip 구조와 비슷하게 구성하려고 하였고, 대부분의 rendering 함수들은 `utils.py`에 있습니다.

## How to run
```
python nerface_trainer.py --config config/dave.txt
```
## TODO
* Training step debugging - GPU가 없어 아직 debugging이 충분히 되지 않았습니다.
* Add validation step - Pytorch Lightning trainer(`nerface_pl.py`)에 validation step 추가하기
* Static background - 논문에서 언급한 static background를 ray의 마지막 위치의 output으로 활용하는 부분 구현
* Add logging, Wandb
* Add latent code - 현재 nerface 페이퍼에서 언급한 latent code가 input으로 들어가고 있지 않습니다.
* Learning rate scheduling - nerface github issue 참고 <https://github.com/gafniguy/4D-Facial-Avatars/issues/3>
* Expression tracker - 현재는 nerface 연구진들이 공개한, 미리 뽑아놓은 Expression vector(76dim)이 dataloader에서 나오게 되어있습니다. 이 부분을 dataloader(image) -> face tracker -> Expression 을 뽑는 방식으로 바꿔야 합니다.
* Image batching (?) - 현재는 한개의 image에서 chunk개(2048)개의 ray를 sampling하고, 각 ray에서 64+128개의 points들이 sampling 되고 있습니다. 즉, 2048 x (64+128)개의 points가 한번에 모델로 들어가게 됩니다. 한번에 여러개의 image를 사용하는 batching을 추가할 수도 있을 것 같은데(필수 x), 이 부분은 기존 nerf-pytorch 코드를 참고하면 됩니다.

## ETC
* nerface팀이 공개한 dave dataset은 nerf-pytorch의 blender type dataset의 형식을 따릅니다.
* 구체적인 seting은 nerface github issue를 참고하면 됩니다. <https://github.com/gafniguy/4D-Facial-Avatars/issues/3>
* coordinate system에 대한 설명입니다. <https://junhahyung.notion.site/coordinate-system-d04a060ae6b54c9d82ae6c1da2ed7a69> 다만 nerface에서는 NDC를 사용하지 않은 것으로 보입니다. 하나 헷갈렸던 점은 camera coordinate system의 경우 right-handed 방식을 따라서, 물체쪽 z-axis가 -z 방향입니다. 이 부분을 알아야 `utils.py`의 `get_rays` 함수를 이해할 수 있습니다.
